---
- hosts: oracledb
  become: yes
  become_method: sudo
  connection: local
  user: oracle

  vars:
      oracle_home: /odb/ods01/orabin/product/18.6.0.0.0_042019_0
      oracle_lib: /odb/ods01/orabin/product/18.6.0.0.0_042019_0/lib
      hostname: "lods0dvd.rnd.pncint.net"
      service_name: dpddb01t.rnd.pncint.net
      user: pdtgops
      password: dtg_0p3rations
      port: '1521'
      oracle_env:
             ORACLE_HOME: "{{oracle_home}}"
             LD_LIBRARY_PATH: "{{oracle_lib}}"

  tasks:
  - name: Run SQL
    oracle_db:
#      username: "{{ user}}"
#      password: "{{ password }}"
      hostname: "{{ hostname }}"
      port: "{{ port }}"
      db_name: "{{ service_name }}"
#      sql: "SELECT * FROM contacts"
    register: results
[pl58688@VM:ldtg006a crz-oracle-healthcheck]$

- name: Executing syninym script on target server
  shell: /bin/sh /odb/ods01/orabin/admin/{{ synonym.sid }}/scripts_automation/synonym.sh
  ignore_errors: false
  register: sql


#!/bin/bash
#spool /odb/ods01/orabin/admin/prtdb07d/scripts_automation/{{ synonym.owner.schema }}_create_public_sync_$date.log

date=$(date +%m%d%Y)

export ORACLE_HOME={{ synonym.config.OracleVersion.oracle_home }}
export ORACLE_SID={{ synonym.sid }}
export ANSIBLE_AUTO_DIR={{ synonym.oracle_base }}/{{ synonym.sid }}/scripts_automation

$ORACLE_HOME/bin/sqlplus -S "/as sysdba " <<EOF > /odb/ods01/orabin/admin/prtdb07d/scripts_automation/{{ synonym.owner.schema }}_create_public_sync_$date.log

@/odb/ods01/orabin/admin/prtdb07d/scripts_automation/{{ synonym.owner.schema }}_create_public_sync.sql

EXIT;
EOF

- name: Run /tmp/create_pnc_users_SI.sql
  oracle_sql_script:
    oracle_home: "{{ oracle_home }}"
    db_name: "{{ dbname }}"
    sql_script: create_pnc_users_SI.sql
    sql_script_path: /tmp/create_pnc_users_SI.sql
    state: script
  register: createpncuserSIresults
  ignore_errors: yes

- name: SQL check for primary or standby
  oracle_sql:
    username: "{{ orclact }}"
    password: "{{ orclpwd }}"
    hostname: "{{ansible_hostname}}"
    port: 1521
    mode: sysdba
    service_name: "{{ dbname }}"
    sql: "SELECT database_role from v$database"
  register: results
  ignore_errors: yes

- name: Run expect to wait for a successful PXE boot via out-of-band CIMC
  shell: |
    set timeout 300
    spawn ssh admin@{{ cimc_host }}

    expect "password:"
    send "{{ cimc_password }}\n"

    expect "\n{{ cimc_name }}"
    send "connect host\n"

    expect "pxeboot.n12"
    send "\n"

    exit 0
  args:
    executable: /usr/bin/expect
  delegate_to: localhost