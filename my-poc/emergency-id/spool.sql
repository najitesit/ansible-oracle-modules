- name: run the sql to get the result
  shell: |
    export ORACLE_SID={{ mysid }}
    export ORACLE_HOME={{ dbhome_locl }}
    {{ dbhome_locl }}/bin/sqlplus -s "/ as sysdba" @/tmp/ansible_sql.sql "{{ myparameter }}"


- name: Execute table.sql using sqlplus 
  shell: $ORACLE_HOME/bin/sqlplus -s username/password@connect @table.sql
  environment:
    ORACLE_HOME: "{{oracle_home_path}}"
    LD_LIBRARY_PATH: "{{ld_library_path}}"
    PATH: "{{bin_path}}"
  args:
    chdir: "{{sql_path}}" 
  become: true
  become_method: su
  become_user: oracle

EXAMPLES = '''
# Execute arbitrary sql
- oracle_sql:
    username: "{{ user }}"
    password: "{{ password }}"
    service_name: one.world
    sql: 'select username from dba_users'
# Execute arbitrary script1
- oracle_sql:
    username: "{{ user }}"
    password: "{{ password }}"
    service_name: one.world
    script: /u01/scripts/create-all-the-procedures.sql
# Execute arbitrary script2
- oracle_sql:
    username: "{{ user }}"
    password: "{{ password }}"
    service_name: one.world
    script: /u01/scripts/create-tables-and-insert-default-values.sql
